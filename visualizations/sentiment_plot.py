import streamlit as st
import altair as alt
import pandas as pd


@st.cache(ttl=24*60*60)
def plot_sentiment(tweets_df_sentiment, time_unit="hoursminutes"):

    if time_unit == "Hours & Minutes":
        time_unit = "hoursminutes"
    if time_unit == "Hours":
        time_unit = "hours"
    if time_unit == "Week":
        time_unit = "monthdate"

    print("time united evaluated to: ", time_unit)
    brush = alt.selection(type='interval', resolve='global')
    polarity_values = alt.Chart(tweets_df_sentiment).mark_point(tooltip=True, size=65, filled=True).encode(
        x=alt.X('created_at:O', timeUnit=time_unit, title="Hour"),
        y=alt.Y('polarity:Q', title=" 🫤 Negative    --     Positive 🙂 ",
                scale=alt.Scale(domain=[-1, 1])), tooltip="text:N").add_selection(brush)

    polarity_average = alt.Chart(tweets_df_sentiment).mark_line(interpolate='catmull-rom', tooltip=True).encode(
        x=alt.X('created_at:O', timeUnit=time_unit, title="Time"),
        y=alt.Y('mean(polarity):Q', title="", scale=alt.Scale(domain=[-1, 1])),
        ).interactive()

    return polarity_values, polarity_average


@st.cache(ttl=24*60*60)
def plot_pie_chart_sentiment(tweets_df_sentiment):

    sentiment_counts = tweets_df_sentiment.groupby(['sentiment']).size()
    source = pd.DataFrame({'sentiment': sentiment_counts.index.values, 'value': sentiment_counts.values})

    pie = alt.Chart(source).mark_arc(outerRadius=120).encode(
        theta=alt.Theta(field="value", type="quantitative"),
        #color=alt.Color(field="sentiment", type="nominal", legend=None)
        color=alt.Color(field="sentiment", type="nominal")
    )
    #legend = pie.mark_text(radius=160, size=20, align='center', dx=-10, dy=4).encode(text="sentiment:N")
    #legend = pie.mark_arc()

    #return pie, legend
    return pie
