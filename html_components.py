empty_box = """<div style="background: ghostwhite; 
            font-size: 20px; 
            padding: 10px; 
            border: 1px solid lightgray; 
            margin: 10px;">
  Placeholder text.
</div>"""

table = """<table>
  <tr>
    <th></th>
    <th scope="col">Tweet</th>
    <th scope="col">Predicted emotion</th>
  </tr>

  <tr>
    <td>1</td>
    <td>GitLab is great!l</td>
    <td>Joy</td>
  </tr>

  <tr>
    <td>2</td>
    <td>Wow one single app? </td>
    <td>Surprise</td>
  </tr>
    
  <tr>
    <td>2</td>
    <td>no more DIY</td>
    <td>fear</td>
  </tr>
</table>"""