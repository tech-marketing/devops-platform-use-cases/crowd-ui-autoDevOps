from transformers import pipeline


def analise_sentiment(text_pd_column):
    all_sentiments = []
    all_scores = []
    classifier = pipeline(task="text-classification", model="cardiffnlp/twitter-roberta-base-sentiment-latest")
    sentiment_predicted = text_pd_column.map(classifier)
    for sentiment in sentiment_predicted:
        all_sentiments.append(sentiment[0]['label'])
        all_scores.append(sentiment[0]['score'])

    return all_sentiments, all_scores

