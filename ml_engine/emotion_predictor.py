from transformers import pipeline
import itertools
import logging



def model_starter():
    pass


def predict_emotion(text_pd_column):
    classifier = pipeline(task="text-classification", model="bhadresh-savani/distilbert-base-uncased-emotion")
    warm_up = classifier("This is great thing")
    logging.info("Model warmed up ", warm_up)
    emotions_predicted = text_pd_column.map(classifier)
    emotions_predicted_flattened = list(itertools.chain(*emotions_predicted))
    labels = [index['label'] for index in emotions_predicted_flattened]
    return labels


