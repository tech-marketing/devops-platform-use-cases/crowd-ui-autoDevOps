from transformers import pipeline
import pandas as pd
import itertools

d = {'text': ['great presentation, good job', 'quite down, tired and yawning', 'wow, that is so cool demo']}
df = pd.DataFrame.from_dict(data=d)

# to do write assertions for
""" [{'label': 'joy', 'score': 0.9968512654304504}]
1    [{'label': 'sadness', 'score': 0.9810096025466...
2      [{'label': 'joy', 'score': 0.9985935091972351}]
"""
print(df)
def emotion_predictor(text_pd_column):
    classifier = pipeline(task="text-classification", model="bhadresh-savani/distilbert-base-uncased-emotion")

    emotions_predicted = text_pd_column.map(classifier)
    #labels = emotions_predicted[0][0]['label']
    #emotions_predicted
    emotions_predicted_flattened = list(itertools.chain(*emotions_predicted))
    labels = [index['label'] for index in emotions_predicted_flattened]

    return labels


emo = emotion_predictor(df['text'])
print(emo)

