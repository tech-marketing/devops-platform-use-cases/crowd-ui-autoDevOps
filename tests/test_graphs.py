import altair as alt
import pandas as pd
import streamlit as st


df = pd.read_csv("../visualizations/dataframe.csv")


def plot_sentiment(tweets_df_sentiment):
    COLOR_RED = "#00C0F2"
    COLOR_BLUE = "#1C83E1"
    polarity_values = alt.Chart(tweets_df_sentiment).mark_point(tooltip=True, size=65, filled=True).encode(
        x=alt.X('created_at:O', timeUnit="hoursminutes", title="Hour"),
        y=alt.Y('polarity:Q', title=" 🫤 Negative    --     Positive 🙂 ", scale=alt.Scale(domain=[-1, 1]))).interactive()

    polarity_average = alt.Chart(tweets_df_sentiment).mark_line(interpolate='catmull-rom', tooltip=True).encode(
        x=alt.X('created_at:O', timeUnit="hoursminutes", title="Hour"),
        y=alt.Y('mean(polarity):Q', title="", scale=alt.Scale(domain=[-1, 1])),
        color=alt.Color(value=COLOR_BLUE)).interactive()

    polarity_average_line = alt.Chart(tweets_df_sentiment).mark_line(interpolate='basis', tooltip=True).encode(
        x=alt.X('created_at:O', timeUnit="hours", title="Hour"),
        y=alt.Y('polarity:Q', title="", scale=alt.Scale(domain=[-1, 1])),
        color=alt.Color(value=COLOR_RED)).interactive()

    #return polarity_values, polarity_average, polarity_average_line

    return polarity_values, polarity_average

#polarity_values, polarity_average, polarity_average_line = plot_sentiment(tweets_df_sentiment=df)
polarity_values, polarity_average = plot_sentiment(tweets_df_sentiment=df)
#st.altair_chart(polarity_values + polarity_average + polarity_average_line, use_container_width=True)
st.altair_chart(polarity_values + polarity_average, use_container_width=True)
hashtags = st.multiselect(label="Filter tweets by Hashtag", options=['#TheOneDevOpsPlatform'],
                          default=["#TheOneDevOpsPlatform"])

print(hashtags[0])
st.write(hashtags)

query = st.text_input(label="", value="#TheOneDevOpsPlatform")
print(query)
st.write("datapoints", len(df))
st.write(polarity_values)
st.write(polarity_average)



def plot_pie_chart_sentiment(tweets_df_sentiment):
    sentiment_counts = tweets_df_sentiment.groupby(['sentiment']).size()
    source = pd.DataFrame({'sentiment': sentiment_counts.index.values, 'value': sentiment_counts.values})

    #return sentiment_counts.plot
    print(sentiment_counts)
    #print(sentiment_counts.shape)
    #print(sentiment_counts['Negative'])
    #print(sentiment_counts.values)
    #sentiment_counts = sentiment_counts.to_frame()
    #print(type(sentiment_counts))
    #print(sentiment_counts.shape)
    #print(sentiment_counts.index.values)
    print(sentiment_counts.values)
    #print(sentiment_counts.columns())
    pie = alt.Chart(source).mark_arc(outerRadius=120).encode(
        theta=alt.Theta(field="value", type="quantitative"),
        color=alt.Color(field="sentiment", type="nominal", legend=None)
    )
    legend = pie.mark_text(radius=150, size=20, align='center', dx=-20).encode(text="sentiment:N")
    return pie, legend

plot_pie, legend = plot_pie_chart_sentiment(tweets_df_sentiment=df)

st.altair_chart(plot_pie + legend)
#if __name__ == "__main__":
    #plot_sentiment(df)