import pandas as pd
import itertools
from transformers import pipeline
from collections import namedtuple




d = {'text': ['great presentation, good job', 'quite down, tired and yawning', 'wow, that is so cool demo']}
df = pd.DataFrame.from_dict(data=d)


def analise_sentiment(text_pd_column):
    classifier = pipeline(task="text-classification", model="cardiffnlp/twitter-roberta-base-sentiment-latest")

    sentiment_predicted = text_pd_column.map(classifier)
    print(sentiment_predicted)
    sentiments_predicted_flattened = list(itertools.chain(*sentiment_predicted))
    sentiment_labels_polarities = [index['label'] for index in sentiments_predicted_flattened]
    return sentiment_labels_polarities


results = analise_sentiment(df["text"])
print(results)
