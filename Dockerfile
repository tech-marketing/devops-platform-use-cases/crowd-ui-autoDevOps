#Dockerfile
LABEL maintainer="William Arias"
FROM python:3.9-slim-bullseye
ARG TWITTER_TOKEN
ENV TWITTER_TOKEN=$TWITTER_TOKEN
COPY .    /app/
WORKDIR /app
RUN pip install -r requirements.txt
EXPOSE 3306/tcp
EXPOSE 5000/tcp

ENTRYPOINT ["streamlit", "run"]
CMD ["app.py", "--server.port", "5000"]
