import requests


def ranking_caller():
    ENDPOINT = "https://us-central1-warias-099fa08d.cloudfunctions.net/ranking-dashboard"
    r = requests.get(ENDPOINT)
    data = r.json()
    indexes = [item['word'] for item in data]
    # list made of dictionaries
    for i, index in enumerate(indexes):
        data[i][index] = data[i]['count']

    return data, indexes


