from UnleashClient import UnleashClient
import streamlit as st



# Initialize the Unleash client
client = UnleashClient(
    url="https://gitlab.com/api/v4/feature_flags/unleash/38667872",
    instance_id="ywrTfTRx9MhsRxQxyzkz",
    app_name="staging",
    disable_metrics=True,
    disable_registration=True
)
client.initialize_client()

if client.is_enabled("input_box"):

    st.markdown("#### Sentiment from most recent tweets")
    st.write("Type in a hashtag to view the latest Twitter sentiment on it")
    query = st.text_input(label="", value="#GitLab")

else:
    st.write("nope")