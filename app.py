import streamlit as st
import pandas as pd
import tweepy
import yaml
import os
import altair as alt
from tweets_utilities.get_tweets import TweetsDataFrame
from visualizations.sentiment_plot import plot_sentiment, plot_pie_chart_sentiment
from streamlit_elements import nivo, elements, mui, dashboard
from service_calls.ranking_call import ranking_caller




class TweetProcessing(tweepy.StreamingClient):

    def on_connect(self):
        print("Connected")

    def on_response(self, response):
        print(f"Tweet Text {response.data.text}")
        data = [response.includes['users'][0].username+" " + response.data.text]
        df = pd.DataFrame(data, columns=['tweet'])
        st.dataframe(df)


def get_bearer_token(data):
    return data["search_tweets_api"]["bearer_token"]


def process_yaml():
    with open("tweets_utilities/config.yaml") as f:
        return yaml.safe_load(f)


def stream_clean_rules(stream_client, bearer_token):
    rules_ids = []
    result = stream_client.get_rules()
    for rule in result.data:
        print(f"rules to be cleaned: {rule.value}")
        rules_ids.append(rule.id)
        print(f"New rules will be added ...")

    if len(rules_ids) > 0:
        stream_client.delete_rules(rules_ids)
        stream_client = TweetProcessing(bearer_token, wait_on_rate_limit=True)
    else:
        print("no rules to clean")

    return stream_client


def breaking_line():
    st.markdown("---")


def main():
    #data = process_yaml()
    #bearer_token = get_bearer_token(data)
    TWITTER_TOKEN = os.environ['TWITTER_TOKEN']

    st.set_page_config(page_title="DevSecOps Platform", page_icon="🏙️", layout="wide")
    title_1, title_2 = st.columns([2, 10])

    with title_1:
        st.text(" ")
        st.image("gitlab-logo-500.png", width=100)

    with title_2:
        st.title("    Audience Tweets Analysis")

    breaking_line()

    # ---------------------------- Instantiate Dataframe with tweets, sentiment and emotion -------------------------
    st.markdown("#### Sentiment from most recent tweets")
    st.write("Type in a hashtag to view the latest Twitter sentiment on it")
    query = st.text_input(label="", value="#KubeCon")
    tweets_df_sentiment, tweets_df_emotion = TweetsDataFrame(hashtag=query, token=TWITTER_TOKEN).create_df()
    #tweets_df_sentiment, tweets_df_emotion = TweetsDataFrame(hashtag=query, token=bearer_token).create_df()

    if tweets_df_sentiment is None:
        st.write("No tweets available with such hashtag 😅")
        st.write(" 🔁 Try another Hashtag")

    else:

        tweets_df_sentiment.to_csv('visualizations/dataframe.csv')

    # ---------------------------- Selecting time frame ---------------------------------
        time_unit = st.radio(
        "Select time lapse",
            ('Hours', 'Hours & Minutes', 'Week'))
    # ---------------------------- Plot Sentiment polarities  -------------------------------------------------------

        #col_1, col_2 = st.columns(2, gap='medium')

        #with col_1:
        if st.button('Submit'):

            tweets_df_sentiment, _ = TweetsDataFrame(hashtag=query, token=TWITTER_TOKEN).create_df()
            polarity_values, polarity_average = plot_sentiment(tweets_df_sentiment=tweets_df_sentiment, time_unit=time_unit)
            st.altair_chart(polarity_values + polarity_average, use_container_width=True)
            breaking_line()

        else:

            polarity_values, polarity_average = plot_sentiment(tweets_df_sentiment=tweets_df_sentiment, time_unit=time_unit)
            st.altair_chart(polarity_values + polarity_average, use_container_width=True)
            breaking_line()


    # ---------------------------- Sentiment Pie Chart  -------------------------------------------------------
        #with col_2:
           # pass

        left, middle, right = st.columns([1, 2, 2], gap="medium")


        with middle:
            st.markdown("## Sentiment comparison")
            #plot_pie, legend = plot_pie_chart_sentiment(tweets_df_sentiment=tweets_df_sentiment)
            plot_pie = plot_pie_chart_sentiment(tweets_df_sentiment=tweets_df_sentiment)
            st.altair_chart(plot_pie)


        with right:
            st.markdown("## Audience Emotions")
            DATA = [
                {"taste": "sadness", "number of tweets": 93},
                {"taste": "joy", "number of tweets": 91},
                {"taste": "love", "number of tweets": 56},
                {"taste": "anger", "number of tweets": 64},
                {"taste": "surprise", "number of tweets": 119},
            ]


            layout_right = [dashboard.Item("rank", 0, 0, 6, 3)]
            with elements("chart"):
                with dashboard.Grid(layout=layout_right, draggableHandle=".draggable"):
                    with mui.Box(sx={"height": 300, '&:hover': {"backgroundColor": '#808080',
                                                                "opacity": [0.9, 0.8, 0.7],
                                                                 "p": 1,
                                                                 "border": '1px dashed grey'}}):
                        nivo.Radar(
                            data=DATA,
                            keys=["number of tweets"],
                            indexBy="taste",
                            valueFormat=">-.2f",
                            margin={"top": 70, "right": 80, "bottom": 40, "left": 80},
                            borderColor={"from": "color"},
                            gridLabelOffset=36,
                            dotSize=10,
                            dotColor={"theme": "background"},
                            dotBorderWidth=2,
                            motionConfig="wobbly",
                            legends=[
                                {
                                    "anchor": "top-left",
                                    "direction": "column",
                                    "translateX": -50,
                                    "translateY": -40,
                                    "itemWidth": 80,
                                    "itemHeight": 20,
                                    "itemTextColor": "#FFFFFF",
                                    "symbolSize": 12,
                                    "symbolShape": "circle",
                                    "effects": [
                                        {
                                            "on": "hover",
                                            "style": {
                                                "itemTextColor": "#000"
                                            }
                                        }
                                    ]
                                }
                            ],
                            theme={
                                "background": "#0E1117",
                                "textColor": "#FFFFFF",
                                "fontSize": 15,
                                "borderColor": "",
                                "tooltip": {
                                    "container": {
                                        "background": "#FFFFFF",
                                        "color": "#31333F",
                                    }
                                }
                            }
                        )





    # ---------------------------- Microservice cards Prediction Dataframe  -------------------------------------------------------
        data, keys = ranking_caller()

        print("data obtained:", data)
        print("keys:", keys)

        layout = [dashboard.Item("ranking", 0, 0, 6, 3),
                  dashboard.Item("sentiment", 6, 0, 6, 3),
                  dashboard.Item("media", 0, 2, 12, 4)]

        with elements("demo"):
            with dashboard.Grid(layout, draggableHandle=".draggable"):

                with mui.Card(key="ranking", sx={"display": "flex", "flexDirection": "column"}):
                    mui.CardHeader(title="StackOverflow Key Terms Ranking ", className="draggable")
                    mui.icon.BarChart(sx={"display": "flex", "ml": 49, "mt": -5.5})

                    with mui.CardContent(sx={"flex": 1, "minHeight": 0}):
                        nivo.Bar(
                            data=data,
                            keys=keys,
                            indexBy='word',
                            valueFormat=">-.2f",
                            margin={"top": 50, "right": 130, "bottom": 50, "left": 60},
                            padding={0.3},
                            borderColor={"from": "color"},
                            dotSize=10,
                            dotColor={"theme": "background"},
                            dotBorderWidth=2,
                            motionConfig="wobbly",
                            legends=[
                                {
                                    "anchor": "top-right",
                                    "direction": "column",
                                    "translateX": 100,
                                    "translateY": -40,
                                    "itemWidth": 80,
                                    "itemHeight": 20,
                                    "itemTextColor": "#999",
                                    "symbolSize": 12,
                                    "symbolShape": "circle",
                                    "effects": [
                                        {
                                            "on": "hover",
                                            "style": {
                                                "itemTextColor": "#000"
                                            }
                                        }
                                    ]
                                }
                            ],
                            theme={
                                "background": "inherit",
                                "textColor": "#FFFFFF",
                                "fontSize": 15,
                                "tooltip": {
                                    "container": {
                                        "background": "#FFFFFF",
                                        "color": "#31333F",
                                    }
                                }
                            }
                        )

                with mui.Card(key="sentiment", sx={"display": "flex", "flexDirection": "column"}):
                    mui.CardHeader(title="Chart", className="draggable")

                    with mui.CardContent(sx={"flex": 1, "minHeight": 0}):
                        nivo.Bar(
                            data=data,
                            keys=keys,
                            indexBy="word",
                            valueFormat=">-.2f",
                            margin={"top": 50, "right": 130, "bottom": 50, "left": 60},
                            padding={0.3},
                            borderColor={"from": "color"},
                            dotSize=10,
                            dotColor={"theme": "background"},
                            dotBorderWidth=2,
                            motionConfig="wobbly",
                            legends=[
                                {
                                    "anchor": "top-right",
                                    "direction": "column",
                                    "translateX": 50,
                                    "translateY": -40,
                                    "itemWidth": 80,
                                    "itemHeight": 20,
                                    "itemTextColor": "#999",
                                    "symbolSize": 12,
                                    "symbolShape": "circle",
                                    "effects": [
                                        {
                                            "on": "hover",
                                            "style": {
                                                "itemTextColor": "#000"
                                            }
                                        }
                                    ]
                                }
                            ],
                            theme={
                                "background": "inherit",
                                "textColor": "#999",
                                "tooltip": {
                                    "container": {
                                        "background": "#FFFFFF",
                                        "color": "#31333F",
                                    }
                                }
                            }
                        )
                        
    # ---------------------------- Emotion Prediction Dataframe  -------------------------------------------------------
        st.markdown("#### Predicted emotions from most recent tweets")

        hashtags = st.multiselect(label="Filter tweets by Hashtag", options=[query],
                              default=[])

        if hashtags:
            st.table(tweets_df_emotion)


    breaking_line()

    # ---------------------------- Near-real time streaming of tweets  -----------------------------------------------

    real_time = st.checkbox("Switch to real-time monitoring")
    if real_time:
        st.markdown("#### Real Time Tweets Emotions 😃")

        streaming_client = TweetProcessing(TWITTER_TOKEN, wait_on_rate_limit=True)
        clean_streaming_client = stream_clean_rules(stream_client=streaming_client, bearer_token=TWITTER_TOKEN)
        clean_streaming_client.add_rules(tweepy.StreamRule(value=query))
        try:
            tweet = clean_streaming_client.filter(expansions="author_id", tweet_fields=["created_at", "author_id"],
                                      user_fields="username")

        except KeyboardInterrupt:
            print("Stopping stream")


if __name__ == "__main__":
    main()
