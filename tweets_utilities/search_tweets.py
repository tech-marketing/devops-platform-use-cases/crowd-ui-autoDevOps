import tweepy
import yaml


class TweetProcessing(tweepy.StreamingClient):

    def on_connect(self):
        print("Connected")

    def on_tweet(self, tweet):

        return tweet.created_at, tweet.text

    def on_includes(self, includes):

        return includes["users"][0].username

    def on_response(self, response):
        print(f"Tweet Text {response.data.text}")
        print(f"Username {response.includes['users'][0].username}")
        return


def get_bearer_token(data):
    return data["search_tweets_api"]["bearer_token"]


def process_yaml():
    with open("config.yaml") as f:
        return yaml.safe_load(f)


def clean_rules(stream_client, bearer_token):
    rules_ids = []
    result = stream_client.get_rules()
    for rule in result.data:
        print(f"rules to be cleaned: {rule.value}")
        rules_ids.append(rule.id)
        print(f"New rules will be added ...")

    if len(rules_ids) > 0:
        stream_client.delete_rules(rules_ids)
        stream_client = TweetProcessing(bearer_token, wait_on_rate_limit=True)
    else:
        print("no rules to clean")

    return stream_client


def main():
    data = process_yaml()
    bearer_token = get_bearer_token(data)
    streaming_client = TweetProcessing(bearer_token, wait_on_rate_limit=True)
    clean_streaming_client = clean_rules(stream_client=streaming_client, bearer_token=bearer_token)
    clean_streaming_client.add_rules(tweepy.StreamRule(value="#TheOneDevOpsPlatform"))
    try:
        tweet = clean_streaming_client.filter(expansions="author_id", tweet_fields=["created_at", "author_id"],
                                      user_fields="username")

    except KeyboardInterrupt:
        print("Stopping stream")


#if __name__ == "__main__":
    #main()