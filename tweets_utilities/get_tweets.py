import pandas as pd
import requests
import tweepy
import logging
from ml_engine.emotion_predictor import predict_emotion
from ml_engine.sentiment_analyzer import analise_sentiment
from tweets_utilities.utilities import score_polarity



class TweetsDataFrame:

    def __init__(self, hashtag, token):
        self.hashtag = hashtag
        self.token = token

    def create_df(self):
        """
        Inputs: Tweets DataFrame Object

        This method:
        1. Instantiates a tweepy client and makes a request based on the hashtag passed to it at instantiation
        2. From the response extracts twitter handle
        3. Creates a data frame with the tweet metadata and emotion predicted

        :return: DataFrame populated with emotion predicted and tweet metadata
        """

        # ------------------------------ Make tweets request ------------------------------
        client = tweepy.Client(bearer_token=self.token, return_type=requests.Response, wait_on_rate_limit=True)
        response = client.search_recent_tweets(query=str(self.hashtag), tweet_fields=["created_at", "author_id"],
                                               max_results=100)
        tweets_dict = response.json()
        logging.info("Response from Twitter received", tweets_dict)

        # ------------------------------ Use `client.get_user` with ID to extract user metadata -----------------------
        try:
            tweets_data = tweets_dict['data']
            for tweet in tweets_data:
                handle = client.get_user(id=tweet['author_id'])
                handle = handle.json()
                tweet['handle'] = handle['data']['username']

        # ------------------------------ Create dataframe inserting Emotion Column  ---------------------------------
            df_all = pd.json_normalize(tweets_data)
            df_all['created_at'] = df_all['created_at'].astype('datetime64[ns]')
            df_emotion = df_all.drop(labels=['author_id', 'id'], axis=1)
            df_emotion = df_emotion[['created_at', 'handle', 'text']]
            df_emotion = df_emotion.head(10)
            df_emotion['predicted emotions'] = predict_emotion(df_emotion['text'])

        # ------------------------------ Create dataframe inserting Sentiment Column  ---------------------------------
        # to do log scores

            df_sentiment = df_all[["created_at", "text"]].copy()

        # Create extra columns in df
            df_sentiment["sentiment"], df_sentiment['scores'] = analise_sentiment(df_sentiment["text"])

            df_sentiment['polarity'] = df_sentiment.apply(lambda row: score_polarity(row), axis=1)

            return df_sentiment, df_emotion

        except KeyError:
            logging.info("No tweets with entered hashtag")

            return None, None










